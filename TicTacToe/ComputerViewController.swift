//
//  ViewController.swift
//  TicTacToe
//
//  Created by Maxim Tripilets on 06.10.2017.
//  Copyright © 2017 Maxim Tripilets. All rights reserved.
//

import UIKit

class ComputerViewController: UIViewController {
    
    @IBOutlet weak var cellBtn0: UIButton!
    @IBOutlet weak var cellBtn1: UIButton!
    @IBOutlet weak var cellBtn2: UIButton!
    @IBOutlet weak var cellBtn3: UIButton!
    @IBOutlet weak var cellBtn4: UIButton!
    @IBOutlet weak var cellBtn5: UIButton!
    @IBOutlet weak var cellBtn6: UIButton!
    @IBOutlet weak var cellBtn7: UIButton!
    @IBOutlet weak var cellBtn8: UIButton!
    @IBOutlet weak var resultLbl: UILabel!
    @IBOutlet weak var winCountLbl: UILabel!
    @IBOutlet weak var drawCountLbl: UILabel!
    @IBOutlet weak var loseCountLbl: UILabel!
    @IBOutlet weak var playAgainBtn: UIButton!
    @IBOutlet weak var saveStatBtn: UIButton!
    
    var origBoard = [Cell.null, Cell.null, Cell.null, Cell.null, Cell.null, Cell.null, Cell.null, Cell.null, Cell.null]
    let huPlayer = Cell.cross
    let aiPlayer = Cell.nought
    var game: Game!
    var iter = 0
    var win = 0
    var draw = 0
    var lose = 0
    var gameState: GameState!
    
    
    @IBAction func saveStat(_ sender: UIButton) {
        
    }
    
    @IBAction func playAgain(_ sender: UIButton) {
        origBoard = [Cell.null, Cell.null, Cell.null, Cell.null, Cell.null, Cell.null, Cell.null, Cell.null, Cell.null]
        game = Game(huPlayer: huPlayer, aiPlayer: aiPlayer)
        gameState = .active
        for i in  0...8 {
            setAIMark(index: i, cell: .null)
        }
        playAgainBtn.isHidden = true
        saveStatBtn.isHidden = true
        resultLbl.isHidden = true
        iter = 0
    }
    
    
    @IBAction func tapMenu(_ sender: UIButton) {
        
    }
    
    


    @IBAction func tapButton(_ sender: AnyObject) {
        if origBoard[sender.tag] == Cell.null && gameState != .finished {
            sender.setImage(UIImage(named: "Cross.png"), for: UIControlState())
            origBoard[sender.tag] = .cross
            iter = iter + 1
            if(iter != 9) {
                let bestSpot = game.minimax(board: origBoard, player: game.aiPlayer)
                setAIMark(index: bestSpot.index!, cell: .nought)
                iter = iter + 1
                print("Spot AI to \(bestSpot.index!) and score \(bestSpot.score!)")
                if bestSpot.score! == 10 {
                    gameState = .finished
                    lose = lose + 1
                    loseCountLbl.text = "\(lose)"
                    resultLbl.text = "Вы проиграли"
                    resultLbl.isHidden = false
                    playAgainBtn.isHidden = false
                    saveStatBtn.isHidden = false
                } else if bestSpot.score! == -10 {
                    gameState = .finished
                    win = win + 1
                    winCountLbl.text = "\(win)"
                    resultLbl.text = "Вы выйграли"
                    resultLbl.isHidden = false
                    playAgainBtn.isHidden = false
                    saveStatBtn.isHidden = false
                }
            } else {
                draw = draw + 1
                drawCountLbl.text = "\(draw)"
                resultLbl.text = "Ничья"
                resultLbl.isHidden = false
                playAgainBtn.isHidden = false
                saveStatBtn.isHidden = false
            }
        } else {
            
        }
    }
    
    func setAIMark(index: Int, cell: Cell) {
        origBoard[index] = cell
        let image: UIImage?
        if cell == .cross {
            image = UIImage(named: "Cross.png")
        } else if cell == .nought {
            image = UIImage(named: "Nought.png")
        } else {
            image = nil
        }
        switch index {
        case 0:
            cellBtn0.setImage(image, for: UIControlState())
        case 1:
            cellBtn1.setImage(image, for: UIControlState())
        case 2:
            cellBtn2.setImage(image, for: UIControlState())
        case 3:
            cellBtn3.setImage(image, for: UIControlState())
        case 4:
            cellBtn4.setImage(image, for: UIControlState())
        case 5:
            cellBtn5.setImage(image, for: UIControlState())
        case 6:
            cellBtn6.setImage(image, for: UIControlState())
        case 7:
            cellBtn7.setImage(image, for: UIControlState())
        case 8:
            cellBtn8.setImage(image, for: UIControlState())
        default:
            print("Wrong index")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        game = Game(huPlayer: huPlayer, aiPlayer: aiPlayer)
        gameState = .active
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

