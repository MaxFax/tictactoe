//
//  GamePlay.swift
//  TicTacToe
//
//  Created by Maxim Tripilets on 08.10.2017.
//  Copyright © 2017 Maxim Tripilets. All rights reserved.
//

import Foundation

enum Cell: Int {
    case null = 0
    case cross = 1
    case nought = 2
}

struct CellScore {
    var index: Int?
    var score: Int?
    init() {}
}

class Game {
    
    var huPlayer: Cell
    var aiPlayer: Cell
    
    init(huPlayer: Cell, aiPlayer: Cell) {
        self.huPlayer = huPlayer
        self.aiPlayer = aiPlayer
    }
    
    func minimax(board: [Cell], player: Cell) -> CellScore {
        var newBoard = board
        let availSpots = emptyIndexies(board: newBoard)
        
        if winning(board: newBoard, player: huPlayer) {
            var winScore = CellScore()
            winScore.score = -10
            return winScore
        } else if winning(board: newBoard, player: aiPlayer) {
            var winScore = CellScore()
            winScore.score = 10
            return winScore
        } else if availSpots.count == 0 {
            var winScore = CellScore()
            winScore.score = 0
            return winScore
        }
        
        var moves = [CellScore]()
        for i in 0 ..< availSpots.count {
            var move = CellScore()
            move.index = availSpots[i]
            newBoard[availSpots[i]] = player
            
            if player == aiPlayer {
                let result = minimax(board: newBoard, player: huPlayer)
                move.score = result.score!
            } else {
                let result = minimax(board: newBoard, player: aiPlayer)
                move.score = result.score!
            }
            
            newBoard[availSpots[i]] = .null
            moves.append(move)
        }
        
        var bestMove = 0
        if player == aiPlayer {
            var bestScore = -10000
            for i in 0 ..< moves.count {
                if let best = moves[i].score, best > bestScore {
                    bestScore = best
                    bestMove = i
                }
            }
        } else {
            var bestScore = 10000
            for i in 0 ..< moves.count {
                if let best = moves[i].score, best < bestScore {
                    bestScore = best
                    bestMove = i
                }
            }
        }
        return moves[bestMove]
    }
    
    func emptyIndexies(board: [Cell]) -> [Int] {
        var indexies = [Int]()
        for (index,cell) in board.enumerated() {
            if cell != Cell.cross && cell != Cell.nought {
                indexies.append(index)
            }
        }
        return indexies
    }
    
    func winning(board: [Cell], player: Cell) -> Bool{
        if (board[0] == player && board[1] == player && board[2] == player) ||
            (board[3] == player && board[4] == player && board[5] == player) ||
            (board[6] == player && board[7] == player && board[8] == player) ||
            (board[0] == player && board[3] == player && board[6] == player) ||
            (board[1] == player && board[4] == player && board[7] == player) ||
            (board[2] == player && board[5] == player && board[8] == player) ||
            (board[0] == player && board[4] == player && board[8] == player) ||
            (board[2] == player && board[4] == player && board[6] == player)
        {
            return true
        } else {
            return false
        }
    }
}
