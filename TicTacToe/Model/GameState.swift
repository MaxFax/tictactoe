//
//  GameState.swift
//  TicTacToe
//
//  Created by Maxim Tripilets on 08.10.2017.
//  Copyright © 2017 Maxim Tripilets. All rights reserved.
//

import Foundation

enum GameState {
    case active
    case finished
}
