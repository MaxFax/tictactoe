//
//  TicTacToeTests.swift
//  TicTacToeTests
//
//  Created by Maxim Tripilets on 06.10.2017.
//  Copyright © 2017 Maxim Tripilets. All rights reserved.
//

import XCTest
@testable import TicTacToe

class TicTacToeTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSetCellId() {
        let cell = Cell(id: 1)
        
        XCTAssertTrue(cell.id > 0)
    }
    
}
